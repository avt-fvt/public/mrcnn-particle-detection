# MRCNN-particle-detection
Mask R-CNN implementation for particle detection.

The instructions for installing and applying MRCNN can be found in the manual folder.

If you want to share your database (manually evaluated images of droplets) with other users, please upload it to the database folder.

## Citation

Use this bibtex to cite our paper:

```
@article{sibirtsev_2023_3,
  author = {Sibirtsev, Stepan and Zhai, Song and Neufang, Mathias and Seiler, Jakob and Jupke, Andreas},
  title = {{Mask R-CNN} based droplet detection in liquid--liquid systems, {Part 3}: Model generalization for accurate processing performance independent of image quality},
  year = {2023},
  journal = {Chemical Engineering Research and Design},
  pages = {161-168},
  volume = {202},
  doi = {10.1016/j.cherd.2023.12.005},
  url = {https://www.sciencedirect.com/science/article/pii/S0263876223007979?dgcid=author},
}

@article{Sibirtsev2023_2,
 author = {Sibirtsev, Stepan and Zhai, Song and Neufang, Mathias and Seiler, Jakob and Jupke, Andreas},
 year = {2023},
 title = {{Mask R-CNN} based droplet detection in liquid--liquid systems, {Part 2}: Methodology for determining training and image processing parameter values improving droplet detection accuracy},
 pages = {144826},
 volume = {473},
 issn = {13858947},
 journal = {Chemical Engineering Journal},
 doi = {10.1016/j.cej.2023.144826},
 url = {https://www.sciencedirect.com/science/article/abs/pii/S138589472303557X}
}

@inproceedings{sibirtsev_2023_1,
 author = {Sibirtsev, Stepan and Zhai, Song and Neufang, Mathias and Seiler, Jakob and Jupke, Andreas},
 title = {{Mask R-CNN} based droplet detection in liquid-liquid systems. {Part 1}: A proof of concept},
 year = {2023},
 publisher = {ISEC},
 booktitle = {Proceedings of International Solvent Extraction Conference},
 pages = {133--139},
 doi = {10.5281/zendo.7907607}, 
 url = {http://media.isec2022.com/2023/05/Final-collected-Proceedings-ISEC-2022.pdf}
}

```
Use this bibtex to cite this repository:

```
@misc{sibirtsev_gitlab_mrcnn,
  title={MRCNN Particle Detection},
  author={Sibirtsev, Stepan},
  year={2023},
  publisher={Github},
  journal={GitHub repository},
  howpublished={\url{https://git.rwth-aachen.de/avt-fvt/public/mrcnn-particle-detection}},
}
```

Use this bibtex to cite the repository this source code is based on:

```
@misc{matterport_maskrcnn_2017,
  title={Mask R-CNN for object detection and instance segmentation on Keras and TensorFlow},
  author={Waleed Abdulla},
  year={2017},
  publisher={Github},
  journal={GitHub repository},
  howpublished={\url{https://github.com/matterport/Mask_RCNN}},
}
```
