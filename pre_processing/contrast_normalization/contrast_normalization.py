import os
import cv2
import numpy as np
from skimage import exposure


# contrast = 1 for clahe
# contrast = 2 for contrast stretching

contrast = 1

# optinal image cropping

crop_image = False
size_y = 1521
size_x = 1931

input_folder = "input"
output_folder = "output"

for filename in os.listdir(input_folder):
    image = cv2.imread(os.path.join(input_folder, filename))
    image_height, image_width, _ = image.shape
    # center crop image to given size
    if crop_image: 
        crop_y = (image_height-size_y)//2
        crop_x = (image_width-size_x)//2
        image = image[crop_y:crop_y+size_y, crop_x:crop_x+size_x]
    original = image.copy()

    if contrast != 0:
        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    if contrast == 1:
        # adaptive Equalization
        image = exposure.equalize_adapthist(image)
        image = image.astype('float32') * 255
    elif contrast == 2:
        # contrast stretching
        p2, p98 = np.percentile(image, (2, 98))
        image = exposure.rescale_intensity(image, in_range=(p2, p98))
    if contrast != 0:
        image = cv2.cvtColor(image, cv2.COLOR_GRAY2BGR)

    cv2.imwrite(os.path.join(output_folder, filename), image)
